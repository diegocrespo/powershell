# Get the directory of the current profile
$ProfileDirectory = Split-Path -Path $PROFILE -Parent

# Construct the full path to CustomFunctions.ps1
$CustomFunctionPath = Join-Path -Path $ProfileDirectory -ChildPath "CustomFunctions.ps1"

# Import the script if it exists
if (Test-Path -Path $CustomFunctionPath) {
    . $CustomFunctionPath
} else {
    Write-Warning "CustomFunctions.ps1 not found at path: $CustomFunctionPath"
}

# Check the OS and set the appropriate prompt function
if ($IsLinux) {
    $env:DOTNET_ROOT = "$HOME/.dotnet"
    $env:PATH += ":$HOME/.dotnet"
    $env:PATH += ":/usr/racket/bin"
    $env:PATH += ":/home/deca/Documents/zig-0.12.0"
    $env:PATH += ":/home/deca/.cargo/bin"
    Set-Alias -Name emacs-gtk -Value '/usr/bin/emacs-29.4-gtk+x11'
    
    Import-Module (Join-Path $ProfileDirectory "./Dec/")
    function prompt { promptLinux }
} elseif ($IsMacOS) {
    function prompt { promptMacOS }
    $env:PYTHONPATH = "/usr/local/bin/python3"
    Set-Alias -Name python -Value '/usr/local/bin/python3'
    Set-Alias -Name pip -Value '/usr/local/bin/pip3'
    Import-Module (Join-Path $ProfileDirectory "./Dec/")
}
