# powershell
My Custom PowerShell profile and modules
## Getting started

Clone this to your `/home/deca/.config/powershell/` if you are on linux. If that is not the correct path check `Split-Path -Parent $PROFILE` to see what the correct path is

## Setting up PowerShell on Linux
1 Check where PowerShell is installed with `which pwsh`.
2. Confirm pwsh is listed in `cat /etc/shells` or `grep /usr/bin/pwsh /etc/shells`
3. run `chsh -s /usr/bin/pwsh`
