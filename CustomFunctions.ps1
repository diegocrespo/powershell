<#
.SYNOPSIS
    Retrieves a formatted list of child items in the current directory.

.DESCRIPTION
    The Get-FormattedChildItem function lists files and folders in the current directory, showing the last write time, size in MB, and name of each item.

.EXAMPLE
    Get-FormattedChildItem

    Lists all items in the current directory with their last write time, size in MB, and name.
#>
function Get-FormattedChildItem {
    Get-ChildItem | Select-Object LastWriteTime, @{Name="Size"; Expression={$_.Length / 1MB}}, Name
}

<#
.SYNOPSIS
    Creates an alias 'lst' for the Get-FormattedChildItem function.
#>
Set-Alias -Name lst -Value Get-FormattedChildItem

<#
.SYNOPSIS
    Lists files and directories sorted by last write time.

.DESCRIPTION
    The lslr function lists all items in the current directory, sorted by their last write time, and displays them in a table format showing their mode, last write time, length, and name.

.EXAMPLE
    lslr

    Displays a sorted list of all items in the current directory in a table format.
#>
function lslr { 
    Get-ChildItem | Sort-Object LastWriteTime | Format-Table -Property Mode, LastWriteTime, Length, Name -AutoSize
}

<#
.SYNOPSIS
    Changes the current directory to the C# programming directory.

.DESCRIPTION
    The csharp function changes the current PowerShell location to the '~/programming/csharp' directory.

.EXAMPLE
    csharp

    Changes the current directory to '~/programming/csharp'.
#>
function csharp {
    Set-Location "~/programming/csharp"
}

<#
.SYNOPSIS
    Runs a C# program in the current directory.

.DESCRIPTION
    The crun function checks for a 'Program.cs' file in the current directory and runs it using 'dotnet run'. If 'Program.cs' is not found, it displays an error message.

.EXAMPLE
    crun

    Executes 'dotnet run' if 'Program.cs' exists in the current directory.
#>
function crun {
    if (Test-Path "Program.cs") {
        dotnet run
    }
    else {
        Write-Host "No Program.cs file found in the current directory."
    }
}

<#
.SYNOPSIS
    Lists only directories in the current path.

.DESCRIPTION
    The lsd function uses Get-ChildItem to list only the directories in the current working directory.

.EXAMPLE
    lsd

    Lists all the directories in the current working directory.
#>
function lsd {
    Get-ChildItem -Directory
}

<#
.SYNOPSIS
    Sets the current location to the directory containing the PowerShell profile.

.DESCRIPTION
    The Set-Profile function changes the current location to the directory where the PowerShell profile file ($PROFILE) is located.

.EXAMPLE
    Set-Profile

    Sets the current location to the directory containing the user's PowerShell profile.
#>

function Set-Profile {
    Set-Location (Split-Path -Path $PROFILE -Parent)
}

function fd {
  Set-Location ~/Games/Emulators/gzdoom-g4.11.3-linux-portable/ && ./gzdoom -file mods/freedoom-0.13.0.zip 
}


<#
.SYNOPSIS
    Customizes the PowerShell prompt for Linux environments.

.DESCRIPTION
    The promptLinux function customizes the PowerShell prompt to display the current path with the user's home directory abbreviated as '~'.

.EXAMPLE
    promptLinux

    Customizes the PowerShell prompt in a Linux environment.
#>
<#
.SYNOPSIS
    Customizes the PowerShell prompt for Linux environments.

.DESCRIPTION
    The promptLinux function customizes the PowerShell prompt to display the current path with the user's home directory abbreviated as '~'.

.EXAMPLE
    promptLinux

    Customizes the PowerShell prompt in a Linux environment.
#>
<#
.SYNOPSIS
    Customizes the PowerShell prompt for Linux environments.

.DESCRIPTION
    The promptLinux function customizes the PowerShell prompt to display the current path with the user's home directory abbreviated as '~'.

.EXAMPLE
    promptLinux

    Customizes the PowerShell prompt in a Linux environment.
#>
function promptLinux {
    $homePath = [System.Environment]::GetFolderPath("UserProfile")
    $currentPath = (Get-Location).Path.Replace($homePath, "~")

    Write-Host ("PS " + $currentPath + ">") -NoNewline
    return " "
}

<#
.SYNOPSIS
    Customizes the PowerShell prompt for macOS environments.

.DESCRIPTION
    The promptMacOS function customizes the PowerShell prompt to display the current path, abbreviating the user's home directory with '~'.

.EXAMPLE
    promptMacOS

    Customizes the PowerShell prompt in a macOS environment.
#>
function promptMacOS {
    $folderName = (Get-Location).Path
    if ($folderName -eq $HOME){
        $shellPath = "~/"
    }
    else{
        $shellPath = $folderName.replace($HOME, "")
    }
    "PS $($shellPath)> "
}
