<#
.SYNOPSIS
Compresses files in a directory using parallel jobs.

.DESCRIPTION
The Compress-Files script compresses all files in a specified directory using parallel jobs. By default, the compressed files are placed in the same directory as the original files. An optional OutDirectory parameter can be used to specify a different output directory.

.PARAMETER Directory
Specifies the directory containing the files to compress.

.PARAMETER OutDirectory
Specifies the output directory where the compressed files should be placed. If not specified, the files will be compressed in the same directory as the original files.

.EXAMPLE
PS> Compress-Files -Directory "C:\path\to\files"

This command compresses all files in the "C:\path\to\files" directory, placing the compressed files in the same directory.

.EXAMPLE
PS> Compress-Files -Directory "C:\path\to\files" -OutDirectory "C:\path\to\compressed_files"

This command compresses all files in the "C:\path\to\files" directory, placing the compressed files in the "C:\path\to\compressed_files" directory.

#>

param (
    [Parameter(Mandatory = $true)]
    [ValidateScript({Test-Path -Path $_ -PathType Container})]
    [string]$Directory,
    [string]$OutDirectory
)

# Get all files in the specified directory
$files = Get-ChildItem -Path $Directory

if ($OutDirectory -eq "") {
    Write-Host "Empty OutDir using $Directory"
    $OutDirectory = $Directory
}

# Create the output directory if it doesn't exist
if ($OutDirectory -ne "") {
    if (-not (Test-Path -Path $OutDirectory -PathType Container)) {
        Write-Host "OutDir does not exist, creating $OutDirectory"
        New-Item -ItemType Directory -Path $OutDirectory | Out-Null
    }
}

# Process each file in parallel using jobs
$jobs = @()
foreach ($file in $files) {

    # 	$fileNameWithoutExtension = [System.IO.Path]::GetFileNameWithoutExtension($file.Name)
    # 	$destinationFileName = $fileNameWithoutExtension + ".zip"
    # 	$destinationPath = Join-Path -Path $OutDirectory -ChildPath $destinationFileName
    # Write-Host "Compress-Archive -Path $(($file).FullName) -DestinationPath $destinationPath -Force"
    $scriptBlock = {
        param ($file, $OutDirectory)
	$fileNameWithoutExtension = [System.IO.Path]::GetFileNameWithoutExtension($file.Name)
	$destinationFileName = $fileNameWithoutExtension + ".zip"
	$destinationPath = Join-Path -Path $OutDirectory -ChildPath $destinationFileName
        # Compress the file
        Compress-Archive -Path $file.FullName -DestinationPath $destinationPath -Force
    }

    $job = Start-Job -ScriptBlock $scriptBlock -ArgumentList $file, $OutDirectory
    $jobs += $job
}

# Wait for all jobs to complete
Wait-Job -Job $jobs | Out-Null

# Clean up completed jobs
$jobs | Remove-Job
