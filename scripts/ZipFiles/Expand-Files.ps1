<#
.SYNOPSIS
Expands zip files in a directory using parallel jobs.

.DESCRIPTION
The Expand-Files script expands all zip files in a specified directory using parallel jobs. By default, the zip files are expanded directly inside the directory where they are located. An optional OutDirectory parameter can be used to specify a different output directory.

.PARAMETER Directory
Specifies the directory containing the zip files to expand.

.PARAMETER OutDirectory
Specifies the output directory where the expanded files should be placed. If not specified, the files will be expanded in the same directory as the zip files.

.EXAMPLE
PS> Expand-Files -Directory "C:\path\to\zip_files"

This command expands all zip files in the "C:\path\to\zip_files" directory, placing the expanded files in the same directory as the zip files.

.EXAMPLE
PS> Expand-Files -Directory "C:\path\to\zip_files" -OutDirectory "C:\path\to\expanded_files"

This command expands all zip files in the "C:\path\to\zip_files" directory, placing the expanded files in the "C:\path\to\expanded_files" directory.

#>

param (
    [Parameter(Mandatory = $true)]
    [ValidateScript({Test-Path -Path $_ -PathType Container})]
    [string]$Directory,
    [string]$OutDirectory
)

# Get all zip files in the specified directory
$zipFiles = Get-ChildItem -Path $Directory -Filter "*.zip"
if ($OutDirectory -eq ""){
    Write-Host "Empty OutDir using $Directory"
    $OutDirectory = $Directory
}

# Create the output directory if it doesn't exist
if ($OutDirectory -ne "") {
    if (-not (Test-Path -Path $OutDirectory -PathType Container)) {
	Write-Host "OutDir does not exists, creating $OutDirectory"
        New-Item -ItemType Directory -Path $OutDirectory | Out-Null
    }
}

# Process each zip file in parallel using jobs
$jobs = @()
foreach ($zipFile in $zipFiles) {
    $scriptBlock = {
        param ($zipFile, $OutDirectory)
        $zipFilePath = $zipFile.FullName
	$destinationPath = $OutDirectory
        # Expand the contents of the zip file
        Expand-Archive -Path $zipFilePath -DestinationPath $destinationPath -Force
    }

    $job = Start-Job -ScriptBlock $scriptBlock -ArgumentList $zipFile, $OutDirectory
    $jobs += $job
}

# Wait for all jobs to complete
Wait-Job -Job $jobs | Out-Null

# Clean up completed jobs
$jobs | Remove-Job
