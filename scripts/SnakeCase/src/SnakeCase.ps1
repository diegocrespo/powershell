param(
    [string]$DirectoryPath,
    [switch]$Verbose
)

function Convert-ToSnakeCase($inputString) {
    $result = ''
    $chars = $inputString.ToCharArray()
    for ($i = 0; $i -lt $chars.Length; $i++) {
        if ($i -gt 0 -and [char]::IsUpper($chars[$i])) {
            $result += "_" + $chars[$i].ToString().ToLower()
        }
        else {
            $result += $chars[$i].ToString().ToLower()
        }
    }
    return $result
}

Get-ChildItem -Path $DirectoryPath | ForEach-Object {
    if (-not $PSItem.PSIsContainer) {
        $originalName = $PSItem.Name
        $newName = Convert-ToSnakeCase $PSItem.BaseName
        if ($PSItem.Extension) {
            $newName += $PSItem.Extension.ToLower()
        }
        
        if ($Verbose) {
	    Write-Output "Renaming '$originalName' to '$newName'"
        }

        Rename-Item $PSItem.FullName -NewName $newName
    }
}
