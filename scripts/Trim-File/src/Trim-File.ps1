param (
    [Parameter(Position=0)]
    [string]$InputFile,

    [Parameter(Position=1)]
    [int]$LinesToTruncate
)

function Truncate-File {
    param(
	[string]$InputFile,
        [int]$LinesToTruncate
    )
    Write-Host $InputFile
    if (-not (Test-Path $InputFile)) {
	Write-Error ("File `{0}` does not exist." -f $InputFile)
	return
    }

    $fileExtension = [System.IO.Path]::GetExtension($InputFile).ToLower()
    $baseFileName = [System.IO.Path]::GetFileNameWithoutExtension($InputFile)
    $directoryPath = [System.IO.Path]::GetDirectoryName($InputFile)
    $outputFile = [System.IO.Path]::Combine($directoryPath, $baseFileName + "_trimmed" + $fileExtension)

    try {
	switch ($fileExtension) {
	    '.csv' {
		$content = Import-Csv $InputFile -ErrorAction Stop
	    }
	    '.tsv' {
		$content = Import-Csv $InputFile -Delimiter "`t" -ErrorAction Stop
	    }
	    '.txt' {
		$content = Get-Content $InputFile -ErrorAction Stop
	    }
	    default {
		Write-Error ("Unsupported file extension `{0}`. The file must be a .csv or .txt ." -f $fileExtension)
	    return
	    }
	}
	$totalLines = $content.Count
	$linesToKeep = $totalLines - $LinesToTruncate

	if ($linesToKeep -lt 0) {
	    Write-Error ("Total Lines : {0}, TruncateLines: {1}. Cannot truncate more lines than the file contains." -f $totalLines, $LinesToKeep)
	}
	else {
	    if ($fileExtension -eq ".csv") {
		$content[0..($linesToKeep)] | Export-Csv $outputFile -NoTypeInformation -Force
	    }

	    elseif ($fileExtension -eq ".tsv") {
		$content[0..($linesToKeep)] | Export-Csv $outputFile -Delimiter "`t" -NoTypeInformation -Force
	    }
	    else{
		$content[0..($linesToKeep)] | Set-Content $outputFile
	    }
	}
    }
    catch {
	Write-Error ("An error occurred while processing the file: $InputFile")
    }
}
# Handle calling script using pester vs in the cmdline
if ($MyInvocation.InvocationName -ne '.') {
    Write-Host "Script is being called as a Standalone."
    Truncate-File -InputFile $InputFile -LinesToTruncate $LinesToTruncate
} else {
    Write-Host "Script is being imported and run"
}

