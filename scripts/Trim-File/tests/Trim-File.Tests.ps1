# Trim-File.Tests.ps1
Describe "Invoke-TruncateFile Functionality" {
    BeforeAll {
        . "$PSScriptRoot/../src/Trim-File.ps1"
    }

    Context "When truncating TXT files" {
        It "Creates a _trimmed version of the file" {
            $inputFile = New-TemporaryFile | Rename-Item -NewName { $_.Name -replace 'tmp$', 'txt' } -PassThru
            $baseFileName = [System.IO.Path]::GetFileNameWithoutExtension($inputFile)
            $directoryPath = [System.IO.Path]::GetDirectoryName($inputFile)
            $expectedOutputFile = [System.IO.Path]::Combine($directoryPath, $baseFileName + "_trimmed.txt")

            "Line1`nLine2`nLine3" | Set-Content -Path $inputFile
            Truncate-File -InputFile $inputFile.FullName -LinesToTruncate 1
            $exists = Test-Path $expectedOutputFile
            $exists | Should -BeTrue
            Remove-Item $inputFile, $expectedOutputFile -Force
        }
    }

    Context "When trimming CSV files with a header" {
        It "Preserves the header and trims the specified number of lines" {
            $inputFile = New-TemporaryFile | Rename-Item -NewName { $_.Name -replace 'tmp$', 'csv' } -PassThru
            $baseFileName = [System.IO.Path]::GetFileNameWithoutExtension($inputFile)
            $directoryPath = [System.IO.Path]::GetDirectoryName($inputFile)
            $expectedOutputFile = [System.IO.Path]::Combine($directoryPath, $baseFileName + "_trimmed.csv")

            "Header1,Header2`nData1,Data2`nData3,Data4`nData5,Data6" | Set-Content -Path $inputFile
            Truncate-File -InputFile $inputFile.FullName -LinesToTruncate 2
            $content = Get-Content $expectedOutputFile
            $content.Count | Should -Be (2 + 1)
            $content[0] | Should -Match '"Header1","Header2"'
            Remove-Item $inputFile, $expectedOutputFile -Force
        }
    }
}
