function New-DecDockerfile {
    <#
    .SYNOPSIS
    This creates a simple dockerfile

    .EXAMPLE
    New-DecDockerfile
    #>
    $dockerfileContent = @"
FROM python:3.12-slim-bookworm
LABEL authors="Diego"
ENV PY_CA_PATH=/usr/local/lib/python3.12/site-packages/certifi/
RUN pip3 install --no-cache-dir azure-ai-formrecognizer==3.3.3 pypdf==3.1.0 openai==1.23.2 langchain==0.1.16 tiktoken==0.6.0

WORKDIR /app/

ADD resources/certs /app/resources/certs
COPY main.py /app/main.py

RUN cat ${PY_CA_PATH}/cacert.pem /app/resources/certs/McKessonEnterpriseRootCAGPT.pem >> /app/resources/certs/allCABundle.pem && \
   cp ${PY_CA_PATH}/cacert.pem ${PY_CA_PATH}/cacert.pem.bkp && \
   cp /app/resources/certs/allCABundle.pem ${PY_CA_PATH}/cacert.pem && rm /app/resources/certs/allCABundle.pem

CMD ["python", "/app/main.py"]
"@

    $dockerfileContent | Out-File -FilePath "Dockerfile" -Encoding UTF8
}
