Function Test-FlatpakInstalled {
    if (-not (Get-Command flatpak -ErrorAction SilentlyContinue)) {
        Write-Error "Flatpak is not installed or not found in PATH. Please install Flatpak first."
        return $false
    }
    return $true
}

Function Get-DecFlatpak {
    <#
    .SYNOPSIS
        Lists installed Flatpak applications. Use Verbose switch to see initial flatpak command
    .DESCRIPTION
        This function retrieves a list of installed Flatpak applications on the system and returns them as objects.
    .EXAMPLE
        Get-DecFlatpak
    .EXAMPLE
        Get-DecFlatpak -Verbose
    .EXAMPLE
        Get-DecFlatpak | Where-Object { $_.Name -eq "Discord" }
    .EXAMPLE
        Get-DecFlatpak | Where-Object { $_.Installation -eq "system" }
    #>
    # needed for the built in parameters like verbose to work
    [CmdletBinding()]
    param()
    if (Test-FlatpakInstalled) {
	Write-Verbose "Executing the command; flatpak list --columns-name,application,version,branch,installation"
        $output = flatpak list --columns=name,application,version,branch,installation |
            ForEach-Object {
                # Remove any ANSI escape sequences if present.
                $line = $_ -replace "`e\[[\d;]*m", ""
		# I can print out these lines if I set $DebugPreference = "continue"""
		# In the terminal
                Write-Debug "Cleaned line: $line"

                # Split the line using tab as the delimiter.
                $columns = $line -split "`t"
                Write-Debug ("Columns: " + ($columns | ForEach-Object { "'$_'" } | Out-String))

                if ($columns.Count -ge 5) {
                    [PSCustomObject]@{
                        Name          = $columns[0].Trim()
                        ApplicationID = $columns[1].Trim()
                        Version       = $columns[2].Trim()
                        Branch        = $columns[3].Trim()
                        Installation  = $columns[4].Trim()
                    }
                }
            }
        return $output
    }
}
