Function Test-FlatpakInstalled {
    if (-not (Get-Command flatpak -ErrorAction SilentlyContinue)) {
        Write-Error "Flatpak is not installed or not found in PATH. Please install Flatpak first."
        return $false
    }
    return $true
}

Function Invoke-DecFlatpak {
    <#
    .SYNOPSIS
        Runs a Flatpak application by name or Application ID (case insensitive).
    
    .DESCRIPTION
        This function searches the installed Flatpak applications for one that matches the provided
        name or Application ID in a case-insensitive manner. If exactly one matching application is found,
        it executes that application using "flatpak run". If no application or more than one application
        matches, an error is thrown. For example, if two applications have the same Name, the function
        will abort and display both Application IDs. Use the Verbose switch to see the executed command
    
    .EXAMPLE
        Invoke-DecFlatpakApp steam
        # Launches the application whose Name matches "steam" (e.g. "Steam").
    
    .EXAMPLE
        Invoke-DecFlatpakApp com.valvesoftware.Steam
        # Launches the application with the Application ID "com.valvesoftware.Steam".
    
    .Example
        Invoke-DecFlatpakApp steam -Verbose

    .NOTES
        - Requires that Flatpak is installed and available in the system PATH.
        - Relies on the Get-DecFlatpak function to retrieve installed Flatpak applications.
    #>
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [string]$App
    )

    # Ensure Flatpak is installed
    if (-not (Test-FlatpakInstalled)) {
        throw "Flatpak is not installed or not found in PATH. Please install Flatpak first."
    }

    # Retrieve the list of installed Flatpak apps.
    $installedApps = Get-DecFlatpak

    # Prepare a case-insensitive search by converting the input to lower case.
    $inputLower = $App.ToLower()

    # First try for an exact match on Name or ApplicationID (ignoring case).
    $exactMatches = $installedApps | Where-Object {
        $_.Name.ToLower() -eq $inputLower -or $_.ApplicationID.ToLower() -eq $inputLower
    }

    # try a match based on full app name
    if ($exactMatches.Count -eq 1) {
        $match = $exactMatches[0]
    }
    # two apps match
    elseif ($exactMatches.count -gt 1) {
	$ids = $exactMatches | ForEach-Object {$_.ApplicationId} -join ", "
	throw "More than one application matches '$App' exactly: $ids"
    }
    else {
        # No exact match is found, try a substring (partial) match.
        $partialMatches = $installedApps | Where-Object {
            $_.Name.ToLower().Contains($inputLower) -or $_.ApplicationID.ToLower().Contains($inputLower)
        }
	if ($partialMatches -eq 0) {
	    throw "No installed Flatpak application matching '$App' was found."
	}
	elseif ($partialMatches -gt 1) {
	    $ids = $partialMatches | ForEach-Object {$_.ApplicationID} -join ", "
            throw "More than one Flatpak application matches '$App'. Matching Application IDs: $ids"
	}
	else {
	    $match = $partialMatches[0]
	}
    }

    Write-Host "Launching $($match.Name) ($($match.ApplicationID))..."

    # If the -Verbose switch is specified, display the flatpak command to be run.
    Write-Verbose "Executing command: flatpak run $($match.ApplicationID)"
    
    flatpak run $match.ApplicationId
}
