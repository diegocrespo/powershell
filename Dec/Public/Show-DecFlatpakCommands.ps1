Function Show-DecFlatpakCommands {
    <#
    .SYNOPSIS
        Displays a reference list of common Flatpak CLI commands for my bad memory.

    .DESCRIPTION
        This function prints out to the terminal a list of the most common Flatpak command-line
        commands. This reference can help you recall or use the standard Flatpak commands directly,
        should you ever need to run the non-PowerShell version of Flatpak.

    .EXAMPLE
        Show-DecFlatpakCommands
        Displays a list of common Flatpak commands.
    #>
    # needed for the built in parameters like verbose to work
    [CmdletBinding()]
    param()

    Write-Host "Common Flatpak CLI Commands:" -ForegroundColor Cyan
    Write-Host "--------------------------------------"
    Write-Host "flatpak list                        - List installed Flatpak applications."
    Write-Host "flatpak run <appID>                 - Run a Flatpak application (e.g., flatpak run com.valvesoftware.Steam)."
    Write-Host "flatpak install <remote> <appID>      - Install a Flatpak application from a specified remote."
    Write-Host "flatpak update                      - Update all installed Flatpak applications."
    Write-Host "flatpak uninstall <appID>           - Uninstall a Flatpak application."
    Write-Host "flatpak info <appID>                - Display detailed information about a Flatpak application."
    Write-Host "flatpak repair                      - Repair any issues with your Flatpak installations."
    Write-Host "flatpak remote-list                 - List all configured Flatpak remotes."
    Write-Host "flatpak remote-add <name> <url>     - Add a new Flatpak remote repository."
    Write-Host "flatpak search <query>              - Search for Flatpak applications."
    Write-Host "flatpak --version                   - Display the installed Flatpak version."
}
