Function Show-DecFlatpakInfo {
    <#
    .SYNOPSIS
        Retrieves and formats detailed information about a specified Flatpak application.

    .DESCRIPTION
        The Show-DecFlatpakInfo cmdlet searches through the installed Flatpak applications (using the 
        Get-DecFlatpak function) to find an application that matches the provided name or Application ID. 
        Once found, it executes the 'flatpak info' command on that application to retrieve detailed information.
        The cmdlet then removes any ANSI escape sequences from the output, parses the output into key/value pairs,
        and returns the information as a PowerShell object. If the first non key–value line is encountered, it is 
        stored as the 'Title' of the application. Verbose output is available to display the executed command.

    .PARAMETER App
        The name or Application ID of the Flatpak application for which to display information.
        The search is performed in a case-insensitive manner. If multiple matches are found, an error is thrown.

    .EXAMPLE
        Show-DecFlatpakInfo -App "steam"
        Retrieves information for the Flatpak application matching "steam" (either by its name or Application ID)
        and returns a PowerShell object with properties such as ID, Ref, Arch, Branch, Version, and more.

    .EXAMPLE
        Show-DecFlatpakInfo -App "com.valvesoftware.Steam"
        Retrieves detailed information for the Flatpak application with the Application ID "com.valvesoftware.Steam".

    .NOTES
        - This cmdlet relies on the Get-DecFlatpak function to retrieve the list of installed Flatpak applications.
        - The output of 'flatpak info' is parsed to remove ANSI escape sequences and to split the output into key/value pairs.
        - If multiple applications match the provided identifier, the cmdlet will throw an error with the matching Application IDs.
        - Verbose output is available with the -Verbose switch.
    
    .LINK
        https://docs.flatpak.org/en/latest/using-flatpak.html (Flatpak Documentation)
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [string]$App
    )

    # Retrieve the list of installed Flatpak apps.
    $installedApps = Get-DecFlatpak

    # Prepare a case-insensitive search by converting the input to lower case.
    $inputLower = $App.ToLower()

    # First try for an exact match on Name or ApplicationID (ignoring case).
    $exactMatches = $installedApps | Where-Object {
        $_.Name.ToLower() -eq $inputLower -or $_.ApplicationID.ToLower() -eq $inputLower
    }

    if ($exactMatches.Count -eq 1) {
        $match = $exactMatches[0]
    }
    elseif ($exactMatches.Count -gt 1) {
        $ids = $exactMatches | ForEach-Object { $_.ApplicationID } -join ", "
        throw "More than one application matches '$App' exactly: $ids"
    }
    else {
        # No exact match is found, try a substring (partial) match.
        $partialMatches = $installedApps | Where-Object {
            $_.Name.ToLower().Contains($inputLower) -or $_.ApplicationID.ToLower().Contains($inputLower)
        }
        if ($partialMatches.Count -eq 0) {
            throw "No installed Flatpak application matching '$App' was found."
        }
        elseif ($partialMatches.Count -gt 1) {
            $ids = $partialMatches | ForEach-Object { $_.ApplicationID } -join ", "
            throw "More than one Flatpak application matches '$App'. Matching Application IDs: $ids"
        }
        else {
            $match = $partialMatches[0]
        }
    }

    Write-Host "Getting info on $($match.Name) ($($match.ApplicationID))..."
    
    # Run the flatpak info command, remove ANSI escape sequences, and filter out blank lines.
    $infoLines = flatpak info $match.ApplicationID | ForEach-Object {
        $_ -replace "`e\[[\d;]*m", ""
    } | Where-Object { $_ -match '\S' }

    # Prepare a hashtable to collect the information.
    $infoObject = @{}
    $firstLine = $true

    foreach ($line in $infoLines) {
        # Try to match lines with "Key: Value" format.
        if ($line -match '^\s*(?<Key>[^:]+):\s*(?<Value>.+)$') {
            $keyName = $matches['Key'].Trim()
            $value = $matches['Value'].Trim()
            $infoObject[$keyName] = $value
            $firstLine = $false
        }
        else {
            # If it's the first non-key line, treat it as a title/description.
            if ($firstLine) {
                $infoObject['Title'] = $line.Trim()
                $firstLine = $false
            }
        }
    }

    # Output the parsed information as a PSCustomObject.
    [pscustomobject]$infoObject
}
