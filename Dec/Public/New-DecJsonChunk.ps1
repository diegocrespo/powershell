Function New-DecJsonChunk {
    <#
    .SYNOPSIS
        Splits a JSON file's specified array value into multiple JSON chunk files and writes to new files.

    .DESCRIPTION
        The New-DecJsonChunk cmdlet reads an input JSON file and extracts the array found at a specified key.
        It then divides this array into a number of approximately equal chunks based on the number provided.
        For example, if the array contains 9 elements and the user specifies 3 splits, each output file will
        contain 3 elements. Each chunk is written to a separate JSON file. The output files are named by taking
        the original file name and appending an underscore followed by a sequential number before the .json extension.
        
        This behavior is useful when you need to break a large JSON array into smaller, more manageable pieces.

    .PARAMETER jsonFilePath
        The path to the input JSON file. The file must exist and contain valid JSON.

    .PARAMETER numberOfSplits
        The number of chunks to split the array into. Must be an integer greater than or equal to 1.

    .PARAMETER key
        The key in the JSON object whose value is an array. This array will be split into chunks.

    .EXAMPLE
        New-DecJsonChunk -jsonFilePath "C:\data.json" -numberOfSplits 3 -key "items"
        This command reads "C:\data.json", extracts the array at the key "items", splits it into 3 chunks,
        and writes the chunks to new files named like "data_1.json", "data_2.json", and "data_3.json".

    .NOTES
        - The input JSON file must be valid.
        - The value at the specified key must be an array.
        - Existing files with the same output names will be overwritten.
        - Verbose output is available when the $VerbosePreference variable is set to 'Continue'.
    #>

    [cmdletbinding()]
    param(
        [Parameter(Mandatory = $true)]
        [ValidateScript({Test-Path $_ -PathType 'Leaf'})]
        [string]$jsonFilePath,
        
        [Parameter(Mandatory = $true)]
        [ValidateRange(1, [int]::MaxValue)]
        [int]$numberOfSplits,
        
        [Parameter(Mandatory = $true)]
        [string]$key
    )
    
    # Read the JSON file and convert from JSON text to a PowerShell object.
    $jsonContent = Get-Content $jsonFilePath -Raw | ConvertFrom-Json

    # Extract the array using the provided key.
    $listToSplit = $jsonContent.$key

    # Validate that the value is indeed an array.
    if (-not ($listToSplit -is [System.Collections.IEnumerable])) {
        Write-Error "The specified key '$key' does not refer to an array."
        return
    }

    # Calculate the size of each chunk using ceiling division.
    $chunkSize = [math]::Ceiling($listToSplit.Count / $numberOfSplits)

    # Iterate over the array and write each chunk to a new file.
    $count = 1
    for ($i = 0; $i -lt $listToSplit.Count; $i += $chunkSize) {
        $chunkFilePath = $jsonFilePath -replace '\.json$', "_$count.json"
        # Ensure the index does not exceed the array's bounds.
        $endIndex = [Math]::Min($i + $chunkSize - 1, $listToSplit.Count - 1)
        $newChunk = $listToSplit[$i..$endIndex]
        $chunkJson = @{
            $key = $newChunk
        } | ConvertTo-Json
        $chunkJson | Out-File -FilePath $chunkFilePath -Encoding utf8

        if ($VerbosePreference -eq 'Continue') {
            Write-Verbose "Writing chunk $count containing $($newChunk.Count) item(s) to $chunkFilePath"
        }
        $count++
    }
}
