Function ConvertTo-DecPrettyJson {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true, Position = 0)]
        [string]$Path,

        [Parameter(Mandatory = $false)]
        [int]$Depth = 100
    )

    <#
    .SYNOPSIS
        Converts a JSON file into a pretty-formatted JSON file.

    .DESCRIPTION
        This function reads an input JSON file, converts it into a pretty (indented) version using the
        specified depth, and writes it to a new file. The new file is saved in the same directory as the
        input file with the suffix ".pretty" inserted before the original file extension. This makes the
        JSON file easier to read in text editors.

    .EXAMPLE
        ConvertTo-DecPrettyJson -Path "C:\Data\config.json"
        # Creates "config.pretty.json" in the C:\Data folder.

    .PARAMETER Path
        The full path to the JSON file to be converted.

    .PARAMETER Depth
        Optional. Specifies the maximum depth for nested objects when converting to JSON. The default is 100.
    #>

    if (-not (Test-Path $Path)) {
        Write-Error "File not found: $Path"
        return
    }

    try {
        # Read the entire file as a single string and convert from JSON.
        $jsonContent = Get-Content -Path $Path -Raw | ConvertFrom-Json
    }
    catch {
        Write-Error "Failed to parse JSON file: $_"
        return
    }

    # Convert the object back to JSON with indentation (pretty-printing).
    $prettyJson = $jsonContent | ConvertTo-Json -Depth $Depth

    # Construct the new file name by inserting ".pretty" before the extension.
    $directory = Split-Path -Parent $Path
    $baseName  = [System.IO.Path]::GetFileNameWithoutExtension($Path)
    $extension = [System.IO.Path]::GetExtension($Path)
    $newFile   = Join-Path -Path $directory -ChildPath ("{0}_pretty{1}" -f $baseName, $extension)

    # Write the pretty-formatted JSON to the new file.
    $prettyJson | Out-File -FilePath $newFile -Encoding utf8

    Write-Output "Pretty JSON saved to: $newFile"
}
