Function New-DecCProject {
    <#
    .SYNOPSIS
        Creates a new C project with a basic directory structure and starter files.

    .DESCRIPTION
        The New-DecCProject cmdlet creates a new folder in the current directory with the specified 
        project name. Inside this folder, it creates a "src" directory containing a starter "main.c" file,
        and it places a Makefile in the project folder. The Makefile is preconfigured with compiler flags and
        build rules for debug and production builds, including support for SDL3 via pkg-config.

    .EXAMPLE
        New-DecCProject -ProjectName "MyCProject"
        This command creates a folder called "MyCProject" in the current directory. Inside "MyCProject", 
        a "src" directory is created with a basic "main.c" file, and a Makefile is placed in the root of 
        the project folder.

    .NOTES
        - Existing files in the target directories may be overwritten.
        - Ensure you have write permissions in the current directory.
    #>

    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true)]
        [string]$ProjectName
    )


    # Get the current directory and build the project path.
    $currentDir = Get-Location
    $projectPath = Join-Path -Path $currentDir -ChildPath $ProjectName

    # Create the project directory if it doesn't exist.
    if (-Not (Test-Path -Path $projectPath)) {
        New-Item -ItemType Directory -Path $projectPath | Out-Null
    }
    else {
        Write-Warning "Directory '$ProjectName' already exists. Files may be overwritten."
    }

    # Create the src directory inside the project folder.
    $srcPath = Join-Path -Path $projectPath -ChildPath "src"
    if (-Not (Test-Path -Path $srcPath)) {
        New-Item -ItemType Directory -Path $srcPath | Out-Null
    }

    # Define the content for main.c using a literal here-string.
    $mainCContent = @'
#include <stdio.h>

int main(void) {
    printf("Hello, world!\n");
    return 0;
}
'@

    $mainCPath = Join-Path -Path $srcPath -ChildPath "main.c"
    $mainCContent | Out-File -FilePath $mainCPath -Encoding UTF8

    # Define the content for the Makefile.
    $makefileContent = @'
# Compiler and flags
CC = gcc
CFLAGS = -std=c17 -Wall -Wextra -pedantic -Werror -Wshadow -Wconversion -Wformat=2 \
         -Wfloat-equal -Wcast-align -Wpointer-arith -Wstrict-overflow=5 -Wwrite-strings \
         -Wundef -Wswitch-default -Wswitch-enum -Wunreachable-code -Wredundant-decls
DEBUG_FLAGS = -O2 -flto -fstack-protector-strong -fsanitize=address -fsanitize=undefined \
              -fsanitize=leak -g3
PROD_FLAGS = -O2 -flto -march=native -fstack-protector-strong -D_FORTIFY_SOURCE=2 \
             -fpie -pie -Wl,-z,relro,-z,now

# Use pkg-config to get SDL3 flags
SDL3_CFLAGS = $(shell pkg-config --cflags sdl3)
SDL3_LIBS = $(shell pkg-config --libs sdl3)

# Project-specific
SRC_DIR = src
OBJ_DIR = obj
BIN = app
SRCS = $(wildcard $(SRC_DIR)/*.c)
OBJS = $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o,$(SRCS))

# Rules
.PHONY: all debug prod clean

all: debug

debug: CFLAGS += $(DEBUG_FLAGS) $(SDL3_CFLAGS)
debug: $(BIN)

prod: CFLAGS += $(PROD_FLAGS) $(SDL3_CFLAGS)
prod: $(BIN)

$(BIN): $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@ $(SDL3_LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c | $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

clean:
	rm -rf $(OBJ_DIR) $(BIN)
'@

    $makefilePath = Join-Path -Path $projectPath -ChildPath "Makefile"
    $makefileContent | Out-File -FilePath $makefilePath -Encoding UTF8

    Write-Host "C project '$ProjectName' created successfully in '$projectPath'."
}
