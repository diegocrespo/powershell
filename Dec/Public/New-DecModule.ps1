Function New-DecModule {

   <#
    .SYNOPSIS
        Creates a new PowerShell module with a standardized folder structure and template files.

    .DESCRIPTION
        The New-DecModule cmdlet creates a new PowerShell module directory using the specified module name.
        It sets up a basic module structure that includes subdirectories for public functions, private functions,
        and tests. Additionally, it generates a main module script that dot-sources all the PowerShell scripts
        found in the Public and Private folders, and a module manifest file (.psd1) that defines the module.

    .PARAMETER ModuleName
        The name of the module to be created. This name is used to generate the module folder, the main module
        script file (ModuleName.psm1), and the module manifest file (ModuleName.psd1).

    .EXAMPLE
        New-DecModule -ModuleName MyModule
        This command creates a new module named "MyModule". It generates the following structure:
            MyModule\
                Public\      # For public function scripts.
                Private\     # For private function scripts.
                Tests\       # For tests.
                MyModule.psm1    # The main module script that dot-sources scripts from Public and Private.
                MyModule.psd1    # The module manifest file.

    .NOTES
        - Ensure you have the necessary file system permissions to create directories and files in the current location.
        - Modify the template content in the main module script as needed for your specific requirements.
    #>
    [cmdletbinding()]
    param (
	[Parameter(Mandatory = $true)]
	[string]$ModuleName
    )

    New-Item -ItemType Directory -Path $ModuleName -Force | Out-Null

    $privateFolder = New-Item -ItemType Directory -Path "$ModuleName\Private" -Force
    $publicFolder = New-Item -ItemType Directory -Path "$ModuleName\Public" -Force
    $testsFolder = New-Item -ItemType Directory -Path "$ModuleName\Tests" -Force

    $moduleManifestPath = "$ModuleName\$moduleName.psd1"
    $moduleScriptPath = "$ModuleName\$moduleName.psm1"

    
    New-Item -ItemType File -Path $moduleScriptPath -Force | Out-Null


    if (Test-Path $moduleScriptPath) {
	$scriptContent =
	  @'
#Get public and private function definition files.
    $Public  = @( Get-ChildItem -Path $PSScriptRoot\Public\*.ps1 -ErrorAction SilentlyContinue )
    $Private = @( Get-ChildItem -Path $PSScriptRoot\Private\*.ps1 -ErrorAction SilentlyContinue )

#Dot source the files
    Foreach($import in @($Public + $Private))
    {
        Try
        {
            . $import.fullname
        }
        Catch
        {
            Write-Error -Message "Failed to import function $($import.fullname): $_"
        }
    }

Export-ModuleMember -Function $Public.Basename
'@
	$scriptContent | Set-Content -Path $moduleScriptPath -Force
    }

    New-ModuleManifest -Path $moduleManifestPath -RootModule "$moduleName.psm1" | Out-Null 
}
