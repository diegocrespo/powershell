Function Expand-DecSingleFiles {
    <#
    .SYNOPSIS
    Expands zip files into a single directory instead of a nested one.

    .PARAMETER InputDirectory
    Specifies the directory containing the zip files to expand.

    .EXAMPLE
    PS> Expand-Files -InputDirectory "C:\path\to\zip_files"

    #>

    [cmdletbinding()]
    param (
	[Parameter(Mandatory = $true)]
	[string]$InputDirectory
    )
    # Create a tempPath based on location of zip file to hold the zip files
    $tempDir = (New-Guid).Guid
    $basePath = Split-Path (Resolve-Path $InputDirectory)
    $tempPath = Join-Path $basePath $tempDir
    Expand-Archive -LiteralPath $InputDirectory -DestinationPath $tempPath
    Get-ChildItem $tempPath -Recurse | Move-Item -Destination $basePath
    Remove-Item $tempPath -Recurse
}
