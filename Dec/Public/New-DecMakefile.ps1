Function New-DecMakefile {
    <#
    .SYNOPSIS
        Creates a skeleton Makefile in the specified directory.

    .DESCRIPTION
        The New-DecMakefile cmdlet generates a basic (skeleton) Makefile with default compiler definitions,
        source and object file directories, and simple build rules. The generated Makefile includes a minimal set
        of targets such as "all" and "clean", along with rules to compile C source files from a "src" directory into
        an "obj" directory and link them into an executable named "app".

    .EXAMPLE
        New-DecMakefile
        Creates a skeleton Makefile in the current directory.

    .EXAMPLE
        New-DecMakefile -Path "C:\MyProject"
        Creates a skeleton Makefile in the specified "C:\MyProject" directory.

    .NOTES
        - If a Makefile already exists in the target directory, it will be overwritten.
        - Customize the Makefile content as needed for your project.
    #>

    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $false)]
        [string]$Path = (Get-Location)
    )


    $makefileContent = @'
# Skeleton Makefile

# Compiler and flags
CC = gcc
CFLAGS = -std=c17 -Wall -Wextra -pedantic -Werror

# Project-specific settings
SRC_DIR = src
OBJ_DIR = obj
BIN = app
SRCS = $(wildcard $(SRC_DIR)/*.c)
OBJS = $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o,$(SRCS))

# Build rules
.PHONY: all clean

all: $(BIN)

$(BIN): $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c | $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

clean:
	rm -rf $(OBJ_DIR) $(BIN)
'@

    # Build the full path for the Makefile.
    $makefilePath = Join-Path -Path $Path -ChildPath "Makefile"

    # Write the skeleton Makefile to disk.
    $makefileContent | Out-File -FilePath $makefilePath -Encoding UTF8

    Write-Host "Skeleton Makefile created at: $makefilePath"
}
