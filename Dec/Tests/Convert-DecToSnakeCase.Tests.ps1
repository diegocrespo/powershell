BeforeAll {
    # Adjust the path as necessary to point to the location of SnakeCase.ps1
    . (Join-Path "$PSScriptRoot" -ChildPath "/../Public/SnakeCase.ps1")
}

describe "Convert-ToSnakeCase Tests" {
    It "converts simple PascalCase to snake_case" {
        $result = Convert-ToDecSnakeCase "PascalCase"
        $result | Should -Be "pascal_case"
    }

    It "converts simple CamelCase to snake_case" {
        $result = Convert-ToDecSnakeCase "camelCase"
        $result | Should -Be "camel_case"
    }

    It "leaves lowercase strings unchanged" {
        $result = Convert-ToDecSnakeCase "lowercase"
        $result | Should -Be "lowercase"
    }

    It "handles strings with multiple uppercase letters correctly" {
        $result = Convert-ToDecSnakeCase "ThisIsATest"
        $result | Should -Be "this_is_a_test"
    }

    It "handles single uppercase letter strings" {
        $result = Convert-ToDecSnakeCase "A"
        $result | Should -Be "a"
    }

    It "handles empty strings correctly" {
        $result = Convert-ToDecSnakeCase ""
        $result | Should -Be ""
    }
}
